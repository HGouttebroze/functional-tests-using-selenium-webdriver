package com.amazon.test;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

	private WebDriver driver;
	private Popup popup;

	@Before
	public void setUp() throws Exception {
		// On récupére le système d'explotation
		String os = System.getProperty("os.name")
				.toLowerCase()
				.split(" ")[0];
		// générer le chemin du fichier du driver
		String pathMarionette = Paths.get(".").toAbsolutePath().normalize().toString()+"/lib/chromedriver-"+os;
		
		// enregistre le chemin dans une propriété qui est webdriver.chrome.driver
		// Firefox : webdriver.gecko.driver
		System.setProperty("webdriver.chrome.driver", pathMarionette);

		// options pour mettre le navigateur en pleine écran
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");

		// on créé l'objet ChromeDriver
		driver = new ChromeDriver(options);
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
		
		// création de l'objet Popup
		popup = new Popup(driver, "a-popover-1", "//*[@id=\"a-popover-1\"]/div/header/button");
	}	
	
	@Test
	  public void testUntitledTestCase() throws Exception {
		// open URL
	    driver.get("https://www.amazon.fr/ref=nav_logo");
	    
	    WebElement cookie = driver.findElement(By.id("sp-cc-accept"));
        cookie.click();
        Thread.sleep(2000);
	    
	    // stock ds 1 variable, on va chercher l'element avec son id (mais on pt le faire avec le xpath, la class, le tag, le selecteur css...)
	    WebElement search = driver.findElement(By.id("twotabsearchtextbox"));// check 1 element rather by ID
	    
	    // click on element that we have checked
	    search.click();// on appel le click mais on peut aussi recup: prendre avec "get...", comme getText (pr recup le text)
	    // attend 1 second
	    Thread.sleep(1000); // pr que le driver se syncronise avec ce que le navigateur va donner comme contenu au web driver.
	    
	    // clean input text
	    search.clear();
	    
	    // on envoie du text à 1 champ textbox, ce text va apparaitre ds l'input de recherche du site, c'est la valeur/les articles qu'on va rechercher
	    search.sendKeys("selenium testing tools cookbook"); // "mots-clés"
	    
	    // on check le btn avec icone loupe qui soumet les info & valide...
	    WebElement submitSearch = driver.findElement(By.id("nav-search-submit-button"));
	    
	    submitSearch.click();
	    
	    Thread.sleep(1000);
	    
	    // Pour parcourir les resultats:
	    // trouver ds l'inspecteur comment recupérer ces resultats, ici, ya; // class="sg-col-inner"
	    WebElement divSearch = driver.findElement(By.className("s-desktop-content"));
	    
	    // recupérer les images qui sont ds la div
	    List<WebElement> imgs = divSearch.findElements(By.tagName("img"));
	    for(WebElement img : imgs) {
	    	// on check contenu de la balise alt, on a des methode pr check de infos...
	    	String alt = img.getAttribute("alt");
	    	// on click sur l'image qui contient
	    	if (alt.toLowerCase().contains("Selenium Testing Tools Cookbook (English Edition)".toLowerCase())) {
				img.click();
				break;
			}
	    }
	    Thread.sleep(2000);
	    
	    //
	    driver.findElement(By.id("a-autoid-7-announce")).click(); // change id name because test failed on href, passed on the parent element, a span
	    Thread.sleep(2000);
	    
	    //driver.findElement(By.id("add-to-cart-button")).click();
	    //Thread.sleep(1000);
	    
	    driver.findElement(By.id("acrCustomerReviewText")).click();
	    //driver.findElement(By.id("hlb-view-cart")).click();
	    Thread.sleep(5000);
	    /*
	    //WebElement spanDelete = driver.findElement(By.className("sc-action-delete"));
	    //spanDelete.findElement(By.tagName("input")).click();
	   // Thread.sleep(2000);
	    */
	    
	    
	    
	    // assertTrue(true); // si bug ??? 
	    /*
	    driver.findElement(By.id("twotabsearchtextbox")).click();
	    driver.findElement(By.id("twotabsearchtextbox")).clear();
	    driver.findElement(By.id("twotabsearchtextbox")).sendKeys("selenium tools cook book");
	    driver.findElement(By.id("nav-search-submit-button")).click();
	    driver.findElement(By.xpath("//div[@id='search']/div/div/div/div/span[3]/div[2]/div[2]/div/span/div/div/div[2]/h2/a/span")).click();
	    driver.findElement(By.id("a-autoid-5-announce")).click();
	    driver.findElement(By.id("add-to-cart-button")).click();
	    driver.findElement(By.id("hlb-view-cart-announce")).click();
	    driver.findElement(By.name("submit.delete.C065b8f72-87c9-420f-a281-3005ae516b7a")).click();
	    */
	  }
	
  	
	@After
	public void tearDown() throws Exception {
		// on ferme le driver ouvert
		driver.close();
		driver.quit();
	}		  

}
